import { Component } from '@angular/core';
import * as firebase from "firebase/app";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor() {
    const firebaseConfig = {
      apiKey: "AIzaSyBYCNcIT0DzKNMFVVt5y9kgU8CgolYGZBc",
      authDomain: "bookshelves2-f9f17.firebaseapp.com",
      databaseURL: "https://bookshelves2-f9f17.firebaseio.com",
      projectId: "bookshelves2-f9f17",
      storageBucket: "bookshelves2-f9f17.appspot.com",
      messagingSenderId: "910846682034",
      appId: "1:910846682034:web:131aacd7b9187191d9faab",
      measurementId: "G-8C6RP1S6F7"
    };
    firebase.initializeApp(firebaseConfig);
  }

}
